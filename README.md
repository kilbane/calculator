# Calculator
This is a simple calculator that I made when I was learning JavaScript. I'm
currently writing tests for it using
[Vitest](https://github.com/vitest-dev/vitest) and
[Cypress](https://github.com/cypress-io/cypress).
